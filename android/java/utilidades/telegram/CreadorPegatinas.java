package utilidades.telegram;


import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;

import java.util.ArrayList;

public class CreadorPegatinas {

    private static final String CREATE_STICKER_PACK_ACTION = "org.telegram.messenger.CREATE_STICKER_PACK";
    private static final String CREATE_STICKER_PACK_EMOJIS_EXTRA = "STICKER_EMOJIS";
    private static final String CREATE_STICKER_PACK_IMPORTER_EXTRA = "IMPORTER";

    private Activity actividad;

    private ArrayList<Uri> stickers;
    private ArrayList<String> emojis;

    public CreadorPegatinas(Activity actividad) {
        this.actividad = actividad;
    }

    /*
    protected void clic_exportar () {


                ArrayList<Uri> uris = new ArrayList<>();
                uris.add(getRawUri("sticker1"));
                uris.add(getRawUri("sticker2"));
                uris.add(getRawUri("sticker3"));
                uris.add(getRawUri("sticker4"));
                uris.add(getRawUri("sticker5"));
                uris.add(getRawUri("sticker6"));
                uris.add(getRawUri("sticker7"));
                uris.add(getRawUri("sticker8"));
                uris.add(getRawUri("sticker9"));

                ArrayList<String> emojis = new ArrayList<>();
                emojis.add("☺️");
                emojis.add("\uD83D\uDE22");
                emojis.add("\uD83E\uDD73");
                emojis.add("\uD83E\uDD2A");
                emojis.add("\uD83D\uDE18️");
                emojis.add("\uD83D\uDE18️");
                emojis.add("\uD83E\uDD2A");
                emojis.add("\uD83E\uDD73");
                emojis.add("☺️");

                doImport(uris, emojis);

        }

     */

        public void agregar (Uri uri, String emoji) {
            stickers.add (uri);
            emojis.add (emoji);
        }

        public void usar (ArrayList<Uri> uris, ArrayList<String> emojis) {
            this.stickers = uris;
            this.emojis = emojis;
        }

    public void usarUris (ArrayList<Uri> uris) {
        this.stickers = uris;
    }

    public void usarEmojis (ArrayList<String> emojis) {
        this.emojis = emojis;
    }


    public void crear () {
        Intent intent = new Intent(CREATE_STICKER_PACK_ACTION);
        intent.putExtra(Intent.EXTRA_STREAM, stickers);
        intent.putExtra(CREATE_STICKER_PACK_IMPORTER_EXTRA, actividad.getPackageName());
        intent.putExtra(CREATE_STICKER_PACK_EMOJIS_EXTRA, emojis);
        intent.setType("image/*");
        try {
            actividad.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
            //no activity to handle intent
        }
    }

    private Uri getRawUri(String filename) {
        return Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + actividad.getPackageName() + "/raw/" + filename);
    }
}
